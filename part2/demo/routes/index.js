var express = require('express');
var router = express.Router();

/* DEMO 0 - halaman index */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Codepolitan Express.js Tutorial Series' });
});

module.exports = router;
