#Express.js Getting Started

Selamat datang di Express.js Getting Started. Ditulis oleh Codepolitan:

###Part 1

instalasi dan dasar - dasar Express.js, seperti routes, cara install package, menerima POST/GET/PUT/DELETE, response json, dan menerima form-data, menampilkan data - data yang dilewatkan ke template, penggunaan Jade dasar.

###Part 2

Memasang web template AdminLTE di Express.js, menggunakan layouting Jade, menggunakan konverter HTML2Jade.com

###Part 2.2

Menggunakan web template EJS untuk penggunaan dasar

###Part3

Membuat halaman login, membuat autentikasi terhadap user yang ada di collection users, memfilter halaman dengan middleware khusus untuk yang login saja, flash message

###Part4

CRUD untuk pengguna aplikasi, membuat validasi form dengan express-validator,

###Part5

CRUD untuk divisi dan jabatan

###part6

CRUD untuk mencatat anggota

###Part7

upload foto anggota, pagination

###Part8 (WIP)

CRUD kepengurusan dan panitia, pagination, AJAX

##Part9 (Soon)

Halaman anggota: akun, edit akun, kontribusi (sudah tergabung di panitia atau pengurus apa) pagination

###Part10 (Soon)

Backup dan cetak file (pdf, csv, excel)

###Part11 (Soon)

Pengaturan Tampilan

Stay up to date here ;()...