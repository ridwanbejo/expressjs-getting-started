var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var anggotaSchema = new Schema({
  nama: { type: String, required: true, unique: true },
  tempat_lahir: String,
  tanggal_lahir: Date,
  alamat: String, // multirecord 
  kontak: String, // multirecord
  riwayat_pendidikan: String, // multirecord
  riwayat_pekerjaan: String, // multirecord
  keterangan: String,
},
{
    timestamps: true
});

var Anggota = mongoose.model('Anggota', anggotaSchema);

module.exports = Anggota;